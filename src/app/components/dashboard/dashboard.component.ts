import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth-service.service";
import {Router} from "@angular/router";
import {DashboardService} from "../../services/dashboard.service";
import {Stat} from "../../models/stat";
import {ChartOptions, ChartType, ChartDataset} from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loader = true;
  stat !: Stat

  barChartOptions: ChartOptions = {
    responsive: true,
  };

  barChartLabels !: string[] ;
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData!: ChartDataset[] ;





  public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];

  //
  public doughnutChartData: ChartDataset[] = [
    { data:  [350, 450, 100], label: 'L\'animalerie' }
  ];

  public doughnutChartType: ChartType = 'doughnut';



  constructor(private authService: AuthService, private router: Router, private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.dashboardService.getStats().subscribe(data => {
      this.stat = data;
      this.loader = false;
    });

    this.dashboardService.getGraph1().subscribe(data => {
      this.barChartLabels = data.label;
      this.barChartData = [
        { data: data.data, label: 'L\'animalerie' }
      ]
    });

    this.dashboardService.getGraph2().subscribe(data => {
        this.doughnutChartLabels = data.label;
        this.doughnutChartData = [
          { data: data.data, label: 'L\'animalerie' }
        ]
    })
  }

  logout(): void {
    this.authService.doLogout();
    this.router.navigate(['/login']);
  }

}
