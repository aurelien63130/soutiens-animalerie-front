import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Stat} from "../models/stat";
import {Graph1} from "../models/graph-1";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  endpoint: string = 'https://localhost:8000/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getStats(): Observable<Stat> {
    return this.http.get<Stat>(this.endpoint + '/stats');
  }

  getGraph1(): Observable<Graph1> {
    return  this.http.get<Graph1>(this.endpoint+ '/graph-1')
  }

  getGraph2(): Observable<Graph1> {
    return  this.http.get<Graph1>(this.endpoint+ '/graph-2')
  }
}
